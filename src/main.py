import script_writer
import theme_generator

if __name__ == "__main__":
    theme = (theme_generator.theme_generator().split('\n'))[1].split(':')[1]
    script_writer.script_generator(theme)